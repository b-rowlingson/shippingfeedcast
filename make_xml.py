#!/usr/bin/env python

import requests
import rfeed
import datetime

from bs4 import BeautifulSoup as bs

FORECAST_URL = "https://www.metoffice.gov.uk/weather/specialist-forecasts/coast-and-sea/print/shipping-forecast"


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
    
import sys

if len(sys.argv) > 1:
    eprint(f"reading from {sys.argv[1]=}")
    html = open(sys.argv[1]).read()
else:
    eprint(f"reading from {FORECAST_URL=}")
    r = requests.get(FORECAST_URL)
    html = r.text

parser = bs(html,'html.parser')

head = parser.find(id="shipping-forecast-heading")
rbox= head.find_next()
p = rbox.findAll("p")
desc = "".join([pp.text for pp in p])


h3s = parser.find_all("h3")[1:] # first h3 is a title

items = []

# general synopsis

h2 = parser.find_all("h2")
syn = list(filter(lambda h:"general synopsis" in h.text, h2))
if(len(syn) == 1):
    synop = rfeed.Item(
        title = syn[0].text,
        description = syn[0].findNext().text,
        pubDate = datetime.datetime.now()
        )
    items.append(synop)
else:
    nosynop = rfeed.Item(
        title="No general synopsis found",
        description = "Possible problem parsing HTML forecast",
        pubDate = datetime.datetime.now()
        )
    items.append(nosynop)
    
        

# warnings at start

firstWarning = parser.find("p", {"class": "warning"})
if firstWarning:
    w = rfeed.Item(
        title="Warnings",
        description = firstWarning.text,
        pubDate = datetime.datetime.now()
        )
    items.append(w)


for h3 in h3s:
    p = h3.findNext()
    item = rfeed.Item(
        title = h3.string,
        description = p.string,
        pubDate = datetime.datetime.now()
        )
    items.append(item)

# gale warnings detail

gw = parser.find(id = "galewarnings")
if gw:
    head = parser.find(id="gale-warnings-heading")

    gwitem = rfeed.Item(
        title = head.text,
        description = gw.text,
        pubDate = datetime.datetime.now()
        )
    gws = gw.find_all("p")
    first=True
    for gwp in gws:
        if first:
            first = False
            next
        area = gwp.find_next().text
        gi = rfeed.Item(
            title=f"Gale Warning: {area}",
            description = gwp.text,
            pubDate = datetime.datetime.now()
            )
        items.append(gi)
#    items.append(gwitem)
    
feed = rfeed.Feed("The Shipping Feedcast",
                  description="Do not use for navigation",
                  items = items,
                  link="http://www.example.com",
                  docs=desc,
                  pubDate = datetime.datetime.now()
                
                  )

rss = feed.rss()
print(rss)

    
